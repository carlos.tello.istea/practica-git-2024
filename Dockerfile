# Utilizamos la imagen oficial de Nginx como base
FROM nginx:latest

# Copiamos nuestro archivo index.html al directorio raíz de Nginx
COPY index.html /usr/share/nginx/html

# Exponemos el puerto 80 para que Nginx pueda ser accesible desde fuera del contenedor
EXPOSE 80
